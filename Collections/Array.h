#pragma once
#include <sal.h>
#include <iostream>
#include <string>

#ifndef DLL_API
#define DLL_API _declspec(dllimport)
#endif // !DLL_API

using namespace std;

namespace collections {

  template<class T>
  class DLL_API Array
  {

    /*
    ** 打印列表中的数据
    ** @param _cout 输出流
    */
    friend ostream& operator<<(
      __in ostream& _cout,
      __in const Array& self
      ) {
      _cout << "[";
      for (int i = 0; i < self.realLength; i++) {
        _cout << self.data[i];
        if (i < self.realLength - 1)
          _cout << ", ";
      }
      _cout << "]";
      return _cout;
    }

  public:
    Array(int length = 8);
    Array(const Array<T>& other);
    ~Array();


    /*
    ** 重载=
    ** @param other 另一个数组对象
    ** @return 新数组
    */
    Array<T>& operator= (
      __in const Array<T>& other
      ) {
      if (this != &other) {
        this->realLength = 0;
        for (int i = 0; i < other.realLength; i++)
          appendData(other.data[i]);
      }
      return *this;
    }

    /*
    ** 获取总长度
    ** @return 当前数组申请的空间总长度
    */
    int getMaxLength();

    /*
    ** 获取已使用数据长度
    ** @return 已使用数据长度
    */
    int getUsedLength();

    /*
    ** 清空数据
    ** 清空数据并不会退还已使用空间
    */
    void clear();

    /*
    ** 添加数据
    ** @param value 要保存的值
    ** @param index 要保存的数据位置(-1向后追加)
    ** @return 已使用数组长度
    */
    int addData(
      __in const T& value,
      __in int index = -1
    );

    /*
    ** 追加元素
    ** @param value 目标元素
    ** @return 当前对象
    */
    Array& appendData(
      __in const T& value
    );

    /*
    ** 重载appendData
    */
    Array& operator+(
      __in const T& value
      );

    /*
    ** 重载appendData
    */
    void operator+=(
      __in const T& value
      );

    /*
    ** 链接两个数组对象
    ** @param other 另一个数组对象
    ** @return 当前对象
    */
    Array& concat(
      __in const Array<T>& other
    );

    /*
    ** 重载concat
    */
    Array& operator+(
      __in const Array<T>& other
      );

    /*
    ** 重载concat
    */
    void operator+=(
      __in const Array<T>& other
      );

    /*
    ** 移除指定元素
    ** @param value 目标元素
    ** @param ret 被移除的元素
    ** @return true-移除成功, false-移除失败
    */
    bool remove(
      __in const T& value,
      __out T& ret
    );

    /*
    ** 移除指定值. 不保证一定成功, 要获取更多信息请使用 bool remove(const T&, T&).
    ** @param value 被移除元素
    ** @return 当前数组对象
    */
    Array& operator-= (
      __in const T& value
      );

    /*
    ** 从当前数组中移除指定内容
    ** @param other 另一个数组
    ** @return 被移除的元素个数
    */
    int remove(
      __in const Array<T>& other
    );

    /*
    ** 移除指定值. 要获取更多信息请使用 int remove(const T&).
    ** @param other 被移除数组
    ** @return 当前数组对象
    */
    Array& operator-=(
      __in const Array<T>& other
      );

    /*
    ** 获取数据
    ** @param index 下标
    ** @param val   值
    ** @return true-成功, false-失败(value 值不可用)
    */
    bool getData(
      __in const int index,
      __out T& val
    );

    /*
    ** 重载获取数据, 获取更多信息请使用 bool getData(const int, T&).
    ** @param index 下标, 下标超出可用值时抛出异常
    ** @return 值
    */
    T& operator[] (
      __in const int index
      );

    /*
    ** 获取数据(不检查边界)
    ** @param index 下标
    ** @return 元素
    */
    int getData(
      __in const int index
    );

    /*
    ** 数组扩容(初始容量的倍数增长)
    ** @param diffSize 当前容量与目标容量差
    ** @return 当前对象
    */
    Array<T>& dilatation(
      __in const int& diffSize
    );

    /*
    ** 数组截取(注意:需要手动释放内存)
    ** @param _start (包含)开始位置, 如果开始位置大于结束位置将获得空的数组对象
    ** @param _end   (排除)结束位置(-1表示截取所有有效数据)
    ** @return 包含指定内容的数组对象, 该对象有效内容长度不会超过当前对象
    */
    Array<T> subArray_s(
      __in int _start = 0,
      __in int _end = -1
    );

    /*
    ** 比较两个数组值是否相等(只比较有效数据)
    ** @param other 另一个数组
    ** @return true-元素相同, false-元素有差异
    */
    bool equals(
      __in const Array<T>& other
    );

    /*
    ** 重载 equals
    */
    bool operator==(
      __in const Array<T>& other
      );

    /*
    ** 重载 equals
    */
    bool operator!=(
      __in const Array<T>& other
      );

    /*
    ** 查找元素
    ** @param value 目标值
    ** @return 元素第一次出现的位置, -1表示未找到
    */
    int indexOf(
      __in const T& value
    );

  private:
    int realLength; // 已使用容量
    int length;     // 最大容量
    T* data;      // 数据仓库
  };


  /************************************************************************/
  /* 实现 */
  /************************************************************************/

  template<class T>
  Array<T>::Array(int length)
  {
    this->realLength = 0;
    this->length = length;
    this->data = new T[length];
  }

  template<class T>
  Array<T>::Array(const Array<T>& other)
  {
    this->realLength = other.realLength;
    this->length = other.length;
    delete[] this->data;
    this->data = new T[length];
    memcpy(this->data, other.data, sizeof(this->data[0]) * this->length);
  }

  template<class T>
  Array<T>::~Array()
  {
    this->length = 0;
    this->realLength = 0;
    delete[] this->data;
  }

  template<class T>
  int Array<T>::getMaxLength()
  {
    return this->length;
  }

  template<class T>
  int Array<T>::getUsedLength()
  {
    return this->realLength;
  }

  template<class T>
  void Array<T>::clear()
  {
    this->realLength = 0;
  }

  template<class T>
  int Array<T>::addData(
    __in const T& value,
    __in int index
  ) {
    if (-1 == index) index = this->realLength;

    int diffSize = this->length - index - 1;
    if (0 > diffSize) dilatation(abs(diffSize));

    // FIXME 如果是Array本身, 会释放两次空间
    this->data[index] = value;
    this->realLength = index + 1;
    return this->realLength;
  }

  template<class T>
  Array<T>& Array<T>::appendData(
    __in const T& value
  ) {
    this->addData(value);
    return *this;
  }

  template<class T>
  Array<T>& Array<T>::operator+(
    __in const T& value
    ) {
    return appendData(value);
  }

  template<class T>
  void Array<T>::operator+=(
    __in const T& value
    ) {
    appendData(value);
  }

  template<class T>
  Array<T>& Array<T>::concat(
    __in const Array<T>& other
  ) {
    int diffLen = this->length - this->realLength - other.realLength;
    if (diffLen < 0)
      dilatation(abs(diffLen));
    memcpy(this->data + realLength, other.data, sizeof(other.data[0]) * other.realLength);
    this->realLength += other.realLength;
    return *this;
  }

  template<class T>
  Array<T>& Array<T>::operator+(
    __in const Array<T>& other
    ) {
    return concat(other);
  }

  template<class T>
  void Array<T>::operator+=(
    __in const Array& other
    ) {
    concat(other);
  }

  template<class T>
  bool Array<T>::remove(
    __in const T& value,
    __out T& ret
  ) {
    memset(&ret, 0, sizeof(int));

    int index = indexOf(value);
    if (-1 == index) return false;

    ret = this->data[index];
    memcpy(this->data + index, this->data + index + 1, sizeof(this->data[0]) * (realLength - index));
    realLength -= 1;
    return true;
  }

  template<class T>
  Array<T>& Array<T>::operator-=(
    __in const T& value
    ) {
    int v;
    remove(value, v);
    return *this;
  }

  template<class T>
  int Array<T>::remove(
    __in const Array<T>& other
  ) {
    int count = 0;
    int delVal = 0;
    if (0 < other.realLength) {
      for (int i = 0; i < other.realLength; i++)
        if (this->remove(other.data[i], delVal))
          count++;
    }
    return 0;
  }

  template<class T>
  Array<T>& Array<T>::operator-=(
    __in const Array<T>& other
    ) {
    remove(other);
    return *this;
  }

  template<class T>
  bool Array<T>::getData(
    __in const int index,
    __out T& val
  ) {
    if (index > this->realLength) {
      return false;
    }
    else {
      val = this->data[index];
      return true;
    }
  }

  template<class T>
  T& Array<T>::operator[](
    __in const int index
    ) {
    if (0 > index || getUsedLength() <= index) {
      throw string("") + "Array index out of bounds[" + to_string(index) + "]";
    }

    T ret;
    getData(index, ret);
    return ret;
  }

  template<class T>
  int Array<T>::getData(
    __in const int index
  ) {
    return this->data[index];
  }

  template<class T>
  Array<T>& Array<T>::dilatation(
    __in const int& diffSize
  ) {
    if (0 < diffSize) {
      length = (2 * length + diffSize - 1) / length * length;
      T* tmp = new T[length];
      memcpy(tmp, data, sizeof(data[0]) * realLength);
      delete[] data;
      data = tmp;
    }
    return *this;
  }

  template<class T>
  Array<T> Array<T>::subArray_s(
    __in int _start,
    __in int _end
  ) {

    if (0 > _start) _start = 0;
    if (-1 == _end || _end > this->realLength) _end = this->realLength;
    if (_end < _start) return Array(1);

    int _size = _end - _start;
    Array ret(_size);
    ret.realLength = _size;

    memcpy(ret.data, this->data + _start, sizeof(this->data[0]) * _size);
    return ret;
  }

  template<class T>
  bool Array<T>::equals(
    __in const Array<T>& other
  ) {
    if (this == &other)
      return true;

    if (this->realLength != other.realLength)
      return false;

    for (int i = 0; i < this->realLength; i++)
      if (this->data[i] != other.data[i])
        return false;

    return true;
  }

  template<class T>
  bool Array<T>::operator==(
    __in const Array<T>& other
    ) {
    return equals(other);
  }

  template<class T>
  bool Array<T>::operator!=(
    __in const Array<T>& other
    ) {
    return !equals(other);
  }


  template<class T>
  int Array<T>::indexOf(
    __in const T& value
  ) {
    for (int i = 0; i < realLength; i++)
      if (value == data[i])
        return i;
    return -1;
  }
}