#include <iostream>
#include "Array.h"
#include <vector>
//#include "Array.cpp"

using namespace std;


// 断言
// c 判断条件
// m 条件不满足时打印的消息
#define assert(c,m) \
          { \
            if (!(c)) \
              cout <<"Err:" << m << endl;\
          }

// 打印空行
#define BR printf("\n");

// 格式化打印
#define PR(fmt,...) \
          { \
            printf("INF:"); \
            printf(fmt, __VA_ARGS__); \
            BR; \
          }

// 打印数组
#define PRS(arr,len) \
        { \
          for (int i=0; i<len; i++) \
            PR("%d = %d", i, arr.getData(i)); \
        }
#define  PRS2(arr) PRS(arr, arr.getUsedLength())
        


int main() {

  Array<int> arr(2);
  assert(2 == arr.getMaxLength(), "最大可用长度不是2");
  assert(0 == arr.getUsedLength(), "初始可用长度不是0");
  assert(1 == arr.addData(10), "调用addData返回值错误");

  arr.addData(1);
  arr.clear();
  assert(0 == arr.getUsedLength(), "初始可用长度不是0");

  arr.addData(1);
  arr.addData(2);
  assert(2 == arr.getMaxLength(), "最大可用长度不是2");
  arr.addData(3);
  arr.addData(4);
  assert(4 == arr.getMaxLength(), "最大可用长度不是4");
  arr.addData(5);
  arr.addData(6);
  assert(8 == arr.getMaxLength(), "最大可用长度不是4?");
  PR("实际可用长度: %d , %d", arr.getMaxLength(), arr.getUsedLength());
  arr.addData(7);
  arr.addData(8);
  arr.addData(9);
  arr.addData(10);
  PR("实际可用长度: %d , %d", arr.getMaxLength(), arr.getUsedLength());

  int v;
  assert(arr.getData(9, v), "下标为9的值获取失败");
  assert(10 == v, "下标为9的值不是10");

  assert(!arr.getData(10, v), "下标为10的值获取成功");
  PR("下标为10的值为: %d", v);

  int rsize = arr.addData(100, 100);
  PR("插入下标 [100] 的值, 已使用:%d", rsize);
  PR("实际长度为: %d", arr.getMaxLength());

  Array<int>& arr1 = arr.appendData(200);
  assert(&arr1 == &arr, "引用的返回数据不正确");

  BR;
  Array<int> arr2(2);
  arr2.appendData(1).appendData(2);
  Array<int> arr3(4);
  arr3.appendData(3).appendData(4).appendData(5);
  arr2.concat(arr3);
  PR("追加后长度 %d, %d", arr2.getUsedLength(), arr2.getMaxLength());
  PRS(arr2, arr2.getUsedLength());

 BR;
 //Array& sub = arr2.subArray_s(0, 10);
 Array<int> sub = arr2.subArray_s(1, 3);
 PRS(sub, sub.getMaxLength());


  BR;
  Array<int> arr4(2);
  arr4.appendData(2).appendData(3);
  PR("两个数组equals? %d", arr4.equals(sub));
  arr4.appendData(4);
  PR("两个数组equals? %d", arr4.equals(sub));

  assert(0 == arr4.indexOf(2), "调用indexOf失败");
  int index = arr4.indexOf(4);
  PR("找到值为4的元素 %d", index);

  BR;
  PRS(arr4, arr4.getUsedLength());
  PR("移除数据: 2,3");
  Array<int> delArr(2);
  delArr.appendData(2).appendData(3);
  arr4.appendData(11).appendData(12);
  PR("移除前 %d %d", arr4.getUsedLength(), arr4.getMaxLength());
  PRS2(arr4);
  arr4.remove(delArr);
  PR("移除后 %d %d", arr4.getUsedLength(), arr4.getMaxLength());
  PRS2(arr4);

  BR;
  Array<int> arr5;
  //arr5 = arr5 + 1;
  arr5 += 1;
  assert(1 == arr5.getUsedLength(), "长度应该是1");
  PRS2(arr5);
  BR
    Array<int> arr6;
  //arr6 = arr6 + arr5;
  arr6 += arr5;
  arr6 = arr6 + 2;
  assert(2 == arr6.getUsedLength(), "长度应该是2");
  PRS2(arr6);

  const int index2 = 1;
  assert(2 == arr6[index2], "下标1的值应该是2");

  Array<Array<int>> a;
  Array<int> b;
  Array<int> c;
  b += 1;
  cout << b << endl;
  c += 2;
  b = c;
  b += 3;
  cout << c << endl;
  cout << b << endl;

  a += b;
  a += c;
  cout << a << endl;

  Array<int> eqArr1;
  eqArr1 += 1;
  Array<int> eqArr2;
  eqArr2 += 1;
  cout << (eqArr1 == eqArr1) << endl;
  cout << (eqArr2 == eqArr1) << endl;

  return 0;
}