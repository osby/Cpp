﻿#include "pch.h"
#include <math.h>
#include <string>

#define DLL_API _declspec(dllexport)
#include "StringBuilder.h"

using namespace std;
using namespace str;


StringBuilder::StringBuilder(const int length) : incrCoefficient(16)
{
  this->maxLen = length;
  this->length = 0;
  this->data = new char[length + 1];
}

StringBuilder::StringBuilder(const char* s) : incrCoefficient(16)
{
  length = strlen(s);
  maxLen = length + 1;
  data = new char[maxLen];
  strcpy_s(data, sizeof(data[0]) * maxLen, s);
}

StringBuilder::StringBuilder(const StringBuilder& s) : incrCoefficient(16)
{
  this->length = 0;
  this->maxLen = 0;
  this->data = new char[1];
  append(s);
}

StringBuilder::~StringBuilder()
{
  maxLen = 0;
  length = 0;
  if (nullptr != data)
    delete[] data;
}

StringBuilder& str::StringBuilder::reset()
{
  memset(data, 0, maxLen);
  maxLen = 0;
  length = 0;
  return *this;
}

StringBuilder& StringBuilder::set(const char* s)
{
  reset();
  append(s);
  return *this;
}

StringBuilder& StringBuilder::append(const char* s)
{
  int len = strlen(s) + 1;
  dalitation(len + length);
  strcat_s(data, maxLen, s);
  data[len] = 0;
  length = len + length;
  return *this;
}

StringBuilder& StringBuilder::append(const int d)
{
  string s = to_string(d);
  return append(s.c_str());
}

StringBuilder& StringBuilder::append(const StringBuilder& s)
{
  append(s.data);
  return *this;
}

char StringBuilder::charAt(const int index)
{
  if (index >= length) {
    StringBuilder e("Index out of bounds");
    e.append(", Expect Index:").append(index);
    e.append(", Max Index:").append(length);
    e.append(".");
    throw e.data;
  }
  return data[index];
}

StringBuilder& StringBuilder::reverse()
{
  int maxIndex = length / 2;
  char tmp;
  for (int i = 0; i < maxIndex; i++) {
    tmp = data[i];
    data[i] = data[length - 1 - i];
    data[length - 1 - i] = tmp;
  }
  return *this;
}

StringBuilder StringBuilder::substring(const int start, const int end)
{
  if (0 > start || length <= end) {
    StringBuilder e("Index out of bounds");
    e.append("[").append(start).append("~").append(end).append("].");
    throw e.data;
  }

  if (start >= end) {
    StringBuilder e("Illegal parameter");
    e.append("[").append(start).append("~").append(end).append("].");
    throw e.data;
  }

  int subLen = end - start;
  StringBuilder ret(subLen);
  strncpy_s(ret.data, sizeof(ret.data[0]) * ret.maxLen + 1, data + start, sizeof(data[0]) * subLen);
  ret.data[subLen] = 0;
  return ret;
}

bool StringBuilder::contains(const char* s)
{
  return strstr(data, s);
}

bool StringBuilder::contains(const StringBuilder& s)
{
  return strstr(data, s.data);
}

int StringBuilder::compare(const char* s)
{
  return strcmp(data, s);
}

int StringBuilder::compare(const StringBuilder& s)
{
  return strcmp(data, s.data);
}

list<StringBuilder> StringBuilder::split(const char* separator)
{
  int len = strlen(separator);
  list<StringBuilder> ret;
  if (0 == len || 0 == this->length) {
    StringBuilder tmp(*this);
    ret.push_back(tmp);
  }
  else {
    char* nextPtr = nullptr;
    char* strToken = strtok_s(data, separator, &nextPtr);
    while (strToken != NULL) {
      StringBuilder tmp(strToken);
      ret.push_back(tmp);
      strToken = strtok_s(nullptr, separator, &nextPtr);
    }
  }

  return ret;
}

const char* StringBuilder::c_str()
{
  return data;
}

void StringBuilder::dalitation(int targetLen)
{
  if (maxLen < targetLen) {
    maxLen = (targetLen + incrCoefficient - 1) / incrCoefficient * incrCoefficient + 1;
    char* old = data;
    data = new char[maxLen];
    memset(data, 0, sizeof(data[0]) * maxLen);
    if (0 < length) strcpy_s(data, maxLen, old);
    if (nullptr != old) 
      delete[] old;
  }
}
