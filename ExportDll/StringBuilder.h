#pragma once
#include <iostream>
#include <list>

#ifndef DLL_API
#define DLL_API _declspec(dllimport)
#endif // !DLL_API

using namespace std;

namespace str {

  class DLL_API StringBuilder
  {

    friend ostream& operator<<(ostream& out, const StringBuilder& self) {
      return out << self.data;
    };

    friend istream& operator>>(istream& in, StringBuilder& self) {
      char* _d = new char[512];
      in >> _d;
      self.length = 0;
      self.append(_d);
      delete[] _d;
      return in;
    }

  private:
    int length; // 数据长度
    char* data; // 数据指针
    int maxLen; // 指针所指向空间大小
    const int incrCoefficient; // 增量系数

  public:
    StringBuilder(const int length = 64);
    StringBuilder(const char* s);
    StringBuilder(const StringBuilder& s);
    ~StringBuilder();

  public:
    StringBuilder& reset();
    StringBuilder& set(const char* s);
    StringBuilder& append(const char* s);
    StringBuilder& append(const int d);
    StringBuilder& append(const StringBuilder& s);
    char charAt(const int index);
    StringBuilder& reverse();
    StringBuilder substring(const int start, const int end);
    bool contains(const char* s);
    bool contains(const StringBuilder& s);
    int compare(const char* s);
    int compare(const StringBuilder& s);
    list<StringBuilder> split(const char* separator);
    const char* c_str();

  private:
    void dalitation(const int targetLen);

  public:
    StringBuilder& operator=(const StringBuilder& s) {
      if (this != &s) {
        length = 0;
        append(s.length > 0 ? s : "");
      }
      return *this;
    }

    StringBuilder& operator=(const char* s)
    {
      reset();
      append(s);
      return *this;
    }

    StringBuilder& operator+(const StringBuilder& s) {
      StringBuilder& ret = append(s);
      return ret;
    }

    StringBuilder& operator+=(const StringBuilder& s) {
      append(s);
      return *this;
    }

    bool operator==(const StringBuilder& s) {
      if (this == &s) return true;
      return 0 == strcmp(data, s.data);
    }

    bool operator!=(const StringBuilder& s) {
      return !(*this == s);
    }

    bool operator<(const StringBuilder& s) {
      return 0 > strcmp(data, s.data);
    }

    bool operator>(const StringBuilder& s) {
      return 0 < strcmp(data, s.data);
    }

    char operator[](const int& i) {
      return charAt(i);
    }
  };
}