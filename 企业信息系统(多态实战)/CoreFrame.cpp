#include "CoreFrame.h"
#include "DBCore.h"
#include "Array.h"
#include "StringBuilder.h"

using namespace collections;
using namespace str;

CoreFrame::CoreFrame()
{
  db = DBCore<Employee>();
}

CoreFrame::~CoreFrame()
{
}

void CoreFrame::start()
{

  int menu = 0;
  while (true) {
    system("cls");

    cout << "欢迎进入企业信息管理系统:" << endl;
    cout << "[1]. 查询所有员工" << endl;
    cout << "[2]. 录入员工信息" << endl;
    cout << "[Q]. 退出" << endl;

    cout << "执行选项:";
    cin >> menu;
    if (0 == menu) break;

    switch (menu)
    {
    case 1:
      showAllEmployees();
      break;
    case 2:
      enterEmployee();
      break;
    default:
      break;
    }
  }

  cout << "Bye." << endl;
}

void CoreFrame::powerOff()
{
  db.powerOff();
}

bool CoreFrame::enterEmployee()
{
  Employee emp;

  cout << "姓名:";
  cin >> emp.name;

  cout << "性别(1-男, 2-女):";
  cin >> emp.birthday;

  cout << "生日:";
  cin >> emp.birthday;

  cout << "确认员工信息是否有误?" << endl;
  cout << emp << endl << "y/n:";
  char ok;
  cin >> ok;
  if ('Y' != ok) {
    db.add(emp);
    return false;
  }
  system("pause");
  return true;
}

void CoreFrame::showAllEmployees()
{
  Array<Employee> arr;
  bool hasData = db.all(arr);
  cout << "员工列表:";
  if (!hasData) {
    cout << "[ 暂无数据 ]" << endl;
  }
  else {
    cout << endl;
    for (int i = 0, len = arr.getUsedLength(); hasData && i < len; i++)
    {
      Employee el = arr[i];
      cout << el << endl;
    }
    cout << endl;
  }

  system("pause");
}
