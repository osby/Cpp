#pragma once
#include "Array.h"

using namespace collections;

template <class T>
class DBCore
{

private:
  Array<T> data;

public:
  DBCore()
  {
    data = 16;
  }

  ~DBCore() 
  {
  }

  bool add(T& bean)
  {
    data.addData(bean);
    return true;
  }

  bool all(Array<T>& ret)
  {
    if (0 < data.getUsedLength()) {
      ret = data;
      return true;
    }
    return false;
  }

  void powerOff()
  {

  }

public:
  DBCore<T>& operator=(const DBCore& other)
  {
    data = other.data;
    return *this;
  }
};
