#pragma once
#include "Employee.h"
#include "DBCore.h"

class CoreFrame
{

private:
  DBCore<Employee> db;

public:
  CoreFrame();
  ~CoreFrame();

public:

  // 显示菜单
  void start();

  // 系统退出, 收尾
  void powerOff();

  // 录入员工信息
  bool enterEmployee();

  // 显示所有员工
  void showAllEmployees();

  //// 查询员工信息
  //Employee findByName(StringBuilder name);

};

