#pragma once
#include <iostream>
#include "StringBuilder.h"

using namespace std;
using namespace str;

class Employee
{

  friend ostream& operator<<(ostream& out, Employee& self)
  {
    return out << self.toString();
  }

public:
  StringBuilder name;
  int gender; // 1:��, 2:Ů
  StringBuilder birthday;

public:
  Employee()
  {
    name = "";
    gender = 1;
    birthday = "";
  }

  Employee(const Employee& other) 
  {
    operator=(other);
  }

  StringBuilder toString()
  {
    StringBuilder ret("");
    ret.append("Employee [")
      .append("name:").append(name).append(",")
      .append("gender:").append(gender).append(",")
      .append("birthday:").append(birthday)
      .append("]");
    return ret;
  }

public:

  Employee& operator=(const Employee& other)
  {
    name = other.name;
    gender = other.gender;
    birthday = other.birthday;
    return *this;
  }

};

