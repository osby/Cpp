#include <iostream>
#include "CSocketProtocol.h"
#include "CSocketProtocolImpl.h"
#include "EncoderDecoderProtocol.h"
#include "HwEncDec.h"
#include "TestFrame.h"

using namespace std;

int main() {

  unsigned char data[1024];
  memset(data, 0, 1024);
  int dataLen = 10;
  memcpy_s(data, 1024, "aabbddaasdfda11111122222", dataLen);

  unsigned char revData[1024];
  memset(revData, 0, 1024);
  int revLen = -1;

  CSocketProtocol* csp  = new CSocketProtocolImpl();
  EncoderDecoderProtocol* encDec = new HwEncDec();
  TestFrame* tf = new TestFrame(csp, encDec);

  int ret = tf->sendAndReceive(data, dataLen, revData, revLen);
  if (0 != ret) {
    cout << "sendAndReceive err:" << ret << endl;
    return ret;
  }
  else {
    cout << "revData: " << revData << endl;
  }
  delete tf;

  return 0;
}
