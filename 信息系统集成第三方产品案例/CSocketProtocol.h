#pragma once
class CSocketProtocol
{

public:
  CSocketProtocol()
  {
  }

  virtual ~CSocketProtocol()
  {
  }

  // 初始化
  virtual int cltSocketInit() = 0;
  // 发送报文
  virtual int cltSocketSend(const unsigned char* data, const int len) = 0;
  // 接受报文
  virtual int cltSocketReceive(unsigned char* data, int* len) = 0;
  // 释放资源
  virtual int cltSocketDestroy() = 0;

};
