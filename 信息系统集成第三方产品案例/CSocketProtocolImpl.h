#pragma once

#include "CSocketProtocol.h"

class CSocketProtocolImpl : public CSocketProtocol
{

private:
  unsigned char* data;
  int len;

public:
  CSocketProtocolImpl();
  ~CSocketProtocolImpl();

public:
  // ͨ�� CSocketProtocol �̳�
  virtual int cltSocketInit() override;
  virtual int cltSocketSend(const unsigned char* data, const int len) override;
  virtual int cltSocketReceive(unsigned char* data, int* len) override;
  virtual int cltSocketDestroy() override;

};

