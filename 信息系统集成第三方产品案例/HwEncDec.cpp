#include "HwEncDec.h"

int HwEncDec::encode(
  const unsigned char* plainData, 
  const int plainLen, 
  unsigned char* encryptData, 
  int* encryptLen
) {
  char tmp;
  for (int i = 0; i < plainLen; i++) {
    encryptData[(*encryptLen)++] = plainData[i]+15;
  }
  return 0;
}

int HwEncDec::decode(
  const unsigned char* encryptData, 
  const int encryptLen, 
  unsigned char* plainData, 
  int* plainLen
) {
  *plainLen = 0;
  char tmp;
  for (int i = 0; i < encryptLen; i++) {
    plainData[(*plainLen)++] = encryptData[i] - 15;
  }
  return 0;
}
