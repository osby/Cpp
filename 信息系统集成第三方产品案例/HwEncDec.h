#pragma once

#include "EncoderDecoderProtocol.h"

class HwEncDec : public EncoderDecoderProtocol
{
public:
  // ����
  virtual int encode(
    const  unsigned char* plainData,
    const int plainLen,
    unsigned char* encryptData,
    int* encryptLen
  );

  // ����
  virtual int decode(
    const  unsigned char* encryptData,
    const int encryptLen,
    unsigned char* plainData,
    int* plainLen
  );
};

