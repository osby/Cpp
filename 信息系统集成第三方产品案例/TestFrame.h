#pragma once
#include "EncoderDecoderProtocol.h"
#include "CSocketProtocol.h"
class TestFrame
{
public:
  TestFrame(CSocketProtocol* sp, EncoderDecoderProtocol* ed);
  ~TestFrame();

  int sendAndReceive(const unsigned char* sendData, const int sendLen, unsigned char* revData, int& revLen);

private:
  CSocketProtocol* sp;
  EncoderDecoderProtocol* ed;
};

