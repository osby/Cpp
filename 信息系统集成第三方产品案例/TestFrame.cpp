#include "TestFrame.h"
#include <iostream>

TestFrame::TestFrame(CSocketProtocol* sp, EncoderDecoderProtocol* ed)
{
  this->sp = sp;
  this->ed = ed;
}

TestFrame::~TestFrame()
{
  if (nullptr != sp) delete sp;
  if (nullptr != ed) delete ed;
}

int TestFrame::sendAndReceive(const unsigned char* sendData, const int sendLen, unsigned char* revData, int& revLen)
{

  unsigned char encData[4096] = { 0 };
  int encLen = 0;

  int ret = sp->cltSocketInit();
  if (0 != ret) {
    printf("csp->cltSocketInit() err: %d", ret);
    goto  End;
  }

  printf("加密前: %s\n", sendData);
  ret = ed->encode(sendData, sendLen, encData, &encLen);
  if (0 != ret) {
    printf("encDec->encode() err: %d", ret);
    goto  End;
  }
  else {
    printf("加密后: %s\n", encData);
  }

  ret = sp->cltSocketSend(encData, sendLen);
  if (0 != ret) {
    printf("csp->cltSocketSend err: %d", ret);
    goto End;
  }


  ret = sp->cltSocketReceive(encData, &encLen);
  if (0 != ret) {
    printf("csp->cltSocketReceive err: %d", ret);
    goto End;
  }

  printf("解密前: %s\n", encData);
  ret = ed->decode(encData, encLen, revData, &revLen);
  if (0 != ret) {
    printf("cspencDec->decode: %d", ret);
    goto End;
  }
  else {
    printf("解密后: %s\n", revData);
  }

  goto End;
End:
  sp->cltSocketDestroy();
  return ret;
}