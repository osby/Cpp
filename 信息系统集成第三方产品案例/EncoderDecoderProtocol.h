#pragma once

class EncoderDecoderProtocol
{
public:
  virtual ~EncoderDecoderProtocol()
  {
  }

public:
  
  // ����
  virtual int encode(
    const  unsigned char* plainData,
    const int plainLen,
    unsigned char* encryptData,
    int* encryptLen
  ) = 0;

  // ����
  virtual int decode(
    const  unsigned char* encryptData,
    const int encryptLen,
    unsigned char* plainData,
    int* plainLen
  ) = 0;

};