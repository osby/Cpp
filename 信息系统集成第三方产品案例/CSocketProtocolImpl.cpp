#include "CSocketProtocolImpl.h"

#include <iostream>



CSocketProtocolImpl::CSocketProtocolImpl()
{
  this->data = nullptr;
  this->len = 0;
}

CSocketProtocolImpl::~CSocketProtocolImpl()
{
  if (nullptr != data)
    delete data;
}

int CSocketProtocolImpl::cltSocketInit()
{
  data = new unsigned char[1024];
  memset(data, 0, 1024);
  len = 0;
  return 0;
}

int CSocketProtocolImpl::cltSocketSend(const unsigned char* data, const int len)
{
  if (nullptr == data)return -1;
  if (0 >= len)return -2;
  memcpy_s(this->data, 1024, data, len);
  this->len = len;
  return 0;
}

int CSocketProtocolImpl::cltSocketReceive(unsigned char* data, int* len)
{
  memcpy_s(data, *len, this->data, this->len);
  *len = this->len;
  return 0;
}

int CSocketProtocolImpl::cltSocketDestroy()
{
  memset(data, 0, 1024);
  len = 0;
  return 0;
}
